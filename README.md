# With Great Power Comes Great Responsibility: Adversarial Machine Learning (AML) 👽 
---


📕 Este repositorio contiene el contenido brindado en la presentación del BlueSpace durante el año 2022. 



![Presentación](./presentation/presentation.PNG)



### Presentación 📋

La encontrarás en la carpeta 📁 `presentation`

### Recursos externos para seguir profundizando 🛠️ 🔧


⌨️ con ❤️ por [Sol](https://twitter.com/solg_sh) 😊